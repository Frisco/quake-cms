<?php if(isset($_SESSION['admin'])) { ?>  
   <div>
      <h3>Pages</h3>
      <?php if(isset($_SESSION['user_message'])) { ?>
         <div id="message">
            <p class="success">
               <?php echo $_SESSION['user_message']; ?>
               <?php unset($_SESSION['user_message']); ?>
            </p>
            <div style='position:absolute; right:5px; top:5px'>
               <a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;' style='color:#333; text-decoration:none'>X</a>
            </div>
         </div>
      <?php } ?>
      <a href="index.php?page=pages&action=create" class="buttons">New Page</a>
      <ul id="pages">
         <?php while($page = mysql_fetch_assoc($page_list->nav)) { ?>
            <li>
               <a href="index.php?page=pages&action=edit&pid=<?php echo $page['id']; ?>" class="page">
                  <?php echo $page['menu_title']; ?>
                  <?php if($page['status'] == '0') { ?>
                     <em>(Draft)</em>
                  <?php } elseif($page['status'] == '1') { ?>
                     <em>(Hidden)</em>
                  <?php } ?>
               </a>
               <a href="/<?php echo $page['slug']; ?>" class="actions view" target="blank">View</a>
               <?php if($page['slug'] != 'index' && $page['slug'] != '404') { ?>
                  <a href="#" name="<?php echo $page['menu_title']; ?>" class="actions delete" id="<?php echo $page['id']; ?>">Delete</a>
               <?php } ?>
            </li>
         <?php } ?>
      </ul>
   </div>
   <script type="text/javascript">
      $(function() {
         $('#message').delay(2000).fadeOut(600);
         
         $(".delete").click(function() {
            
            var delconf = confirm("Are you sure you want to PERMANENTLY DELETE the following page: " + event.target.name + "? THIS ACTION CANNOT BE UNDONE!")
            
            if(delconf) {
               this.parentNode.style.display="none";

               $.ajax({
                  type: "POST",
                  url: "delete.php",
                  data: "pid=" + event.target.id
               });

               return false;
            }
         });  
      });
   </script>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>