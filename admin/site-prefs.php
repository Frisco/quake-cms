<?php if(isset($_SESSION['admin'])) { ?>   
   <h3>Site Preferences</h3>
   <form method="post">
      <table>
         <tr>
            <td>Site Name: </td>
            <td><input type="text" name="sitename" value="<?php echo $prefs['site_name']; ?>" /></td>
         </tr>
         <tr>
            <td>Site Slogan: </td>
            <td><input type="text" name="slogan" value="<?php echo $prefs['site_slogan']; ?>" /></td>
         </tr>
         <tr>
            <td>Layout: </td>
            <td>
               <select name="layout">
                  <?php
                  $layouts = glob(LAYOUTPATH . DS . '*', GLOB_ONLYDIR);
                  foreach($layouts as $layout) {
                     if(file_exists($layout . DS . 'index.tpl.php')) {
                        $layout = str_replace(LAYOUTPATH . DS, '', $layout); ?>
                        <option <?php if($prefs['layout'] == $layout) { ?>selected="selected"<?php } ?>>
                           <?php echo $layout; ?>
                        </option>
                     <?php
                     } else {
                        // Probably want to set an error here about no index.tpl.php
                     } // end if/else ?>
                  <?php } // end foreach ?>
               </select>
            </td>
         </tr>
         <tr>
            <td>Timezone: </td>
            <td>
               <select name="timezone">
                  <option value="14" <?php if($prefs['timezone'] == 14) { ?>selected="selected"<?php } ?>>+14</option>
                  <option value="13" <?php if($prefs['timezone'] == 13) { ?>selected="selected"<?php } ?>>+13</option>
                  <option value="12" <?php if($prefs['timezone'] == 14) { ?>selected="selected"<?php } ?>>+12</option>
                  <option value="11" <?php if($prefs['timezone'] == 11) { ?>selected="selected"<?php } ?>>+11</option>
                  <option value="10" <?php if($prefs['timezone'] == 10) { ?>selected="selected"<?php } ?>>+10</option>
                  <option value="9" <?php if($prefs['timezone'] == 9) { ?>selected="selected"<?php } ?>>+9</option>
                  <option value="8" <?php if($prefs['timezone'] == 8) { ?>selected="selected"<?php } ?>>+8</option>
                  <option value="7" <?php if($prefs['timezone'] == 7) { ?>selected="selected"<?php } ?>>+7</option>
                  <option value="6" <?php if($prefs['timezone'] == 6) { ?>selected="selected"<?php } ?>>+6</option>
                  <option value="5" <?php if($prefs['timezone'] == 5) { ?>selected="selected"<?php } ?>>+5</option>
                  <option value="4" <?php if($prefs['timezone'] == 4) { ?>selected="selected"<?php } ?>>+4</option>
                  <option value="3" <?php if($prefs['timezone'] == 3) { ?>selected="selected"<?php } ?>>+3</option>
                  <option value="2" <?php if($prefs['timezone'] == 2) { ?>selected="selected"<?php } ?>>+2</option>
                  <option value="1" <?php if($prefs['timezone'] == 1) { ?>selected="selected"<?php } ?>>+1</option>
                  <option value="0" <?php if($prefs['timezone'] == 0) { ?>selected="selected"<?php } ?>>UTC</option>
                  <option value="-1" <?php if($prefs['timezone'] == -1) { ?>selected="selected"<?php } ?>>-1</option>
                  <option value="-2" <?php if($prefs['timezone'] == -2) { ?>selected="selected"<?php } ?>>-2</option>
                  <option value="-3" <?php if($prefs['timezone'] == -3) { ?>selected="selected"<?php } ?>>-3</option>
                  <option value="-4" <?php if($prefs['timezone'] == -4) { ?>selected="selected"<?php } ?>>-4</option>
                  <option value="-5" <?php if($prefs['timezone'] == -5) { ?>selected="selected"<?php } ?>>-5</option>
                  <option value="-6" <?php if($prefs['timezone'] == -6) { ?>selected="selected"<?php } ?>>-6</option>
                  <option value="-7" <?php if($prefs['timezone'] == -7) { ?>selected="selected"<?php } ?>>-7</option>
                  <option value="-8" <?php if($prefs['timezone'] == -8) { ?>selected="selected"<?php } ?>>-8</option>
                  <option value="-9" <?php if($prefs['timezone'] == -9) { ?>selected="selected"<?php } ?>>-9</option>
                  <option value="-10" <?php if($prefs['timezone'] == -10) { ?>selected="selected"<?php } ?>>-10</option>
                  <option value="-11" <?php if($prefs['timezone'] == -11) { ?>selected="selected"<?php } ?>>-11</option>
               </select>
            </td>
         </tr>
         <tr>
            <td>Copyright:</td>
            <td><input type="text" name="copyright" value="<?php echo $prefs['copyright']; ?>" /></td>
         </tr>
         <tr>
            <td colspan="2"><input type="submit" name="prefs" value="Save" /></td>
         </tr>
      </table>
   </form>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>