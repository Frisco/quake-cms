<?php
   /*
      CMS Configuration File
      Sets most of the config info for the CMS system
      2011-2012 Chris Clower
      chris@clowerweb.com
   */

   define('VERSION', '0.4.2 Pre Alpha (Dev)');

   // All of our absolute folder paths
   define('ROOTPATH', realpath(dirname( __FILE__)));
   define('DS', '/');
   define('SYSPATH', ROOTPATH . DS. 'system');
   define('ADMINPATH', ROOTPATH . DS. 'admin');
   define('EXTPATH', ROOTPATH . DS. 'extensions'); 
   define('LAYOUTPATH', ROOTPATH . DS. 'layouts');
   define('BROWSERROOT', '/');

   // DB info
   define('DB_HOSTNAME', 'localhost');
   define('DB_USERNAME', 'root');
   define('DB_PASSWORD', '');
   define('DB_DATABASE', 'fob');
?>
