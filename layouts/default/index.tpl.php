<?php session_start(); ?>
<!DOCTYPE HTML>
<html>
   <head>
      <?php if($pdata['title']) { ?>
         <title>
            <?php echo $pdata['title']; ?>
            <?php if($pdata['status'] == '0') { echo ' (Draft)'; } ?>
            <?php if($prefs['site_name']) { echo ' &mdash; ' . $prefs['site_name']; } ?>
         </title>
      <?php } ?>
      <?php if($pdata['keywords']) { ?>
         <meta name="keywords" content="<?php echo $pdata['keywords']; ?>" />
      <?php } ?>
      <?php if($pdata['description']) { ?>
         <meta name="description" content="<?php echo $pdata['description']; ?>" />
      <?php } ?>
      <link rel="stylesheet" type="text/css" href="<?php echo BROWSERROOT . 'layouts/' . $prefs['layout'] . '/css/style.css'; ?>" />
      <!--[if lt IE 9]>
      <script type="text/javascript" src="<?php echo BROWSERROOT . 'layouts/' . $prefs['layout'] . '/js/html5shim.js'; ?>"></script>
      <![endif]-->
   </head>
   <body>
      <div id="main">
         <header>
            <?php if($prefs['site_name']) { ?>
               <h1> <?php echo $prefs['site_name']; ?></h1>
            <?php } ?>
            <?php if($prefs['site_slogan']) { ?>
               <h2><?php echo $prefs['site_slogan']; ?></h2>
            <?php } ?>
         </header>
         <nav>
            <ul>
               <?php while($nav = mysql_fetch_assoc($nav_main->nav)) { ?>
                  <li><a href="/<?php echo ($nav['slug'] == 'index' ? '' : $nav['slug']); ?>"><?php echo $nav['menu_title']; ?></a></li>
               <?php } ?>
            </ul>
         </nav>
         <div id="content">
            <article>
               <?php if($pdata['heading']) { ?>
                  <h3><?php echo $pdata['heading']; ?></h3>
               <?php } ?>
               <?php if($pdata['content']) { echo $pdata['content']; } ?>
               <p id="page-meta">
                  Page created on <?php echo $pdata['creation_date']; ?><br />
                  <?php if($pdata['modified_date']) { ?>
                  Last updated on <?php echo $pdata['modified_date']; ?>
                  <?php } ?>
               </p>
            </article>
         </div>
         <footer>
            <?php if($prefs['copyright']) { ?>
            <p id="copyright"><?php echo $prefs['copyright']; ?></p>
            <?php } ?>
            <p id="powered">Powered by <a href="http://www.quakecms.com/">Quake CMS</a></p>
            <p id="timer">Page generated in: <?php $timer->ShowTime(); ?> seconds.</p>
         </footer> <!-- /main -->
      </div>
   </body>
</html>