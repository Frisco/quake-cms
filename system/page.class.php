<?php
   /*
      CMS Page Class
      Handles general page functions (display, create, update, delete)
      2011-2012 Chris Clower
      chris@clowerweb.com
   */

   class Page {
      private $slug;
      private $title;
      private $keywords;
      private $description;
      private $menu_title;
      private $heading;
      private $content;
      private $sql;
      private $file;
      private $sitename;
      private $slogan;
      private $layout;
      private $timezone;
      private $copyright;
      private $total  = 0;
      private $status = 0;
      private $order  = 0;

      public  $page;
      public  $tpl;
      public  $prefs;

      public function GetPage($admin = 0, $page = NULL, $action = NULL, $pid = NULL) {
         $query = new Query();

         if($admin == 0) {
            if($page) {
               $this->query = $query->Query('select', '*', 'pages', 'slug', $page);
            } else {
               $this->query = $query->Query('select', '*', 'pages', 'slug', 'index');
            }

            $this->total = mysql_num_rows($query->result);

            if($this->total == 0) {
               $this->query = $query->Query('select', '*', 'pages', 'slug', '404');
            }

            $this->page = mysql_fetch_array($query->result);
         } else {
            if(!$page) {
               $this->file = 'dashboard.php';
            }elseif(!$action) {
               $this->file = $page . '.php';
            } else {
               $this->file = $action . '.php';
            }

            if($pid) {
               $this->sql   = $query->Query('select', '*', $page, 'id', $pid);
               $this->total = mysql_num_rows($query->result);
               $this->page  = mysql_fetch_array($query->result);
            }

            if(file_exists($this->file)) {
               $this->tpl = $this->file;
            } else {
               $this->tpl = '404.php';
            }
         }
      }

      public function Prefs() {
         // We want preferences!
         $query = new Query();

         $this->prefs = $query->Query('select', '*', 'prefs');
         $this->prefs = mysql_fetch_array($query->result);

         if(isset($_POST['prefs'])) {
            $this->sitename  = $_POST['sitename'];
            $this->slogan    = $_POST['slogan'];
            $this->layout    = $_POST['layout'];
            $this->timezone  = $_POST['timezone'];
            $this->copyright = $_POST['copyright'];

            // Run the SQL to update prefs
            $this->sql = "UPDATE `prefs` SET
               `site_name` = '" . $this->sitename . "',
               `site_slogan` = '" . $this->slogan . "',
               `layout` = '" . $this->layout . "',
               `timezone` = '" . $this->timezone. "',
               `copyright` = '" . $this->copyright. "
            '";

            mysql_query($this->sql);
         }
      }

      public function Action($page = NULL, $action = NULL, $pid = NULL) {
         // If we're doing stuff with pages and we've submitted...
         if($page == 'pages') {
            if(isset($_POST['slug'])) {
               // Cleaning up the slug input so that we can make proper clean URLs
               $url = new URL();
               $this->slug = $url->ClanURL($_POST['slug']);
               $this->slug = $url->slug;

               $tidy = new HTMLTidy();

               $this->content = mysql_real_escape_string($tidy->Tidy($_POST['content']));
               $this->content = $tidy->content;

               $this->title       = mysql_real_escape_string($_POST['title']);
               $this->keywords    = mysql_real_escape_string($_POST['keywords']);
               $this->description = mysql_real_escape_string($_POST['description']);
               $this->menu_title  = mysql_real_escape_string($_POST['menu_title']);
               $this->heading     = mysql_real_escape_string($_POST['heading']);
               $this->status      = mysql_real_escape_string($_POST['status']);
               $this->order       = mysql_real_escape_string($_POST['order']);
            }

            switch($action) {
               // If we're creating a new page...
               case 'create':
                  if(isset($_POST['slug'])) {
                     $this->sql = "INSERT INTO `pages` (`slug`, `title`, `keywords`,
                        `description`, `menu_title`, `heading`, `content`, `status`,
                        `order`, `creation_date`) VALUES ('" . $this->slug . "',
                        '" . $this->title . "', '" . $this->keywords . "',
                        '" . $this->description . "', '" . $this->menu_title . "',
                        '" . $this->heading . "', '" . $this->content . "',
                        '" . $this->status . "', '" . $this->order . "',
                        UTC_TIMESTAMP())
                     ";

                     mysql_query($this->sql);
                     $_SESSION['user_message'] = 'The page was successfully created!';
                  }
               break;
               // If we're editing a page...
               case 'edit':
                  if(isset($_POST['slug'])) {
                     $this->sql = "UPDATE `pages` SET
                        `slug` = '" . $this->slug . "',
                        `title` = '" . $this->title . "',
                        `keywords` = '" . $this->keywords . "',
                        `description` = '" . $this->description. "',
                        `menu_title` = '" . $this->menu_title . "',
                        `heading` = '" . $this->heading . "',
                        `content` = '" . $this->content . "',
                        `status` = '" . $this->status . "',
                        `order` = '" . $this->order . "',
                        `modified_date` = UTC_TIMESTAMP()
                        WHERE id = '$pid'
                     ";

                     mysql_query($this->sql);
                  }
               break;
            } // End Switch
         } // End if Pages == 'pages'
      } // End Action()
   }
?>