<?php
   /*
      CMS Nav Class
      Gets the nav request and sets the nav accordingly
      2011-2012 Chris Clower
      chris@clowerweb.com
   */

   class Nav {
      public $nav;

      public function GetNav($nav) {
         $query = new Query();

         switch($nav) {
             case'main':
                // Main nav doesn't show hidden or draft pages
                $query->Query('select', '*', 'pages', 'status', '2', 'order', 'ASC');
             break;
             case 'all':
                // Lists all pages regardless of status
                $query->Query('select', '*', 'pages', '', '', 'order', 'ASC');
             break;
         }

         $this->nav = $query->result;
      }
   }
?>
