<?php
   /*
      CMS URL Class
      Grabs stuff from the URL and decides what needs to be done
      2011-2012 Chris Clower
      chris@clowerweb.com
   */

   class URL {
      private $getpage;
      private $err;
      private $urls;
      
      public  $action;
      public  $pid;
      public  $url;
      public  $slug;
      public  $page;
      public  $admin = 0;
      
      public function GetURL() {
         if(isset($_GET['page']))
            $this->page = $_GET['page'];
         
         if(isset($_GET['action']))
            $this->action = $_GET['action'];
         
         if(isset($_GET['pid']))
            $this->pid = $_GET['pid'];
      }
      
      public function SetURL($page = NULL, $action = NULL, $pid = NULL) {
         if($page || $action || $pid) { // Page or action or pid
            if($page && !$action && !$pid) { // Page by itself
               $this->getpage = $page;
            } elseif($page && $action && !$pid) { // Page and action
               $this->getpage = $page . DS . $action;
            } elseif($page && $action && $pid) { // Action, page and pid
               $this->getpage = $page . DS . $action . DS . $pid;
            } elseif(!$page && $action) { // Action without a page
               $this->err = 'Can\'t have an action without a page!';
            } elseif(!$page && !$action && $pid) { // pid by itself
               $this->err = 'Can\'t have a pid without a page and action!';
            } elseif(!$page && $action && $pid) { // action and pid but no page
               $this->err = 'Can\'t have a pid and action without a page!';
            } elseif($page && !$action && $pid) { // page with action but no pid
               $this->err = 'Can\'t have a page with pid without an action!';
            }
            
            // Yep, if we have these query strings, we're accessing admin pages
            $this->admin = 1;
         } else { // We're not on an admin page, just get the URI
            if(!$this->admin)
               $this->getpage = $_SERVER['REQUEST_URI'];
            else
              $this->err = 'The URL is malformed!';
         }

         if(!$this->err) {
            $this->url = ltrim($this->getpage, '/');
            $this->url = explode('/', $this->url);
            
            if(!$this->admin) {             
               foreach($this->url as $part) {
                  $this->urls = $this->urls . $part . '/';
               }

               $this->url = rtrim($this->urls, '/');
               
               if($this->url == 'admin')
                  $this->admin = 1;
            }
         } else {
            echo 'An error occurred: ' . $this->err;
         }
      }
      
      public function ClanURL($url) {
         if($url) {
            $this->slug = iconv('UTF-8', 'ASCII//TRANSLIT', $url);
            $this->slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $this->slug);
            $this->slug = strtolower(trim($this->slug, '-'));
            $this->slug = preg_replace("/[\/_|+ -]+/", '-', $this->slug);
         }
      }
   }
?>